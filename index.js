const express = require("express");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const {AgregarRegistro, BuscarRegistro} = require("./controllers/Lean");


dotenv.config();
if(!process.env.PORT){
    console.error("Falta el puerto");
    process.exit();
}


let app = express();
let router = express.Router();
app.use(bodyParser.urlencoded({ extended:false}));
app.use(bodyParser.json());

app.use('/', router);
router.get('/', (req,res)=>{
    res.send(`Pusiste el codigo ${req.query.codigo}`);
})

router.get('/datos/:nombre', async(req,res)=>{
    let registro = await BuscarRegistro(req.params.nombre);
    res.status(200).json(registro);
})


router.post('/datos', async (req,res)=>{
    let resultado = await AgregarRegistro(req.body);
    res.status(resultado.status).send(resultado.text);
    
});

mongoose.connect('mongodb://52.188.114.190:27017/',  { useNewUrlParser: true, useUnifiedTopology: true }, async (err) =>{
    console.log("Conectando");
    if(err){
        console.log(`No anduvo porque ${err}`);
        return false;
    }
    app.listen(process.env.PORT, ()=>{
        console.log(`Se está ejecutando el servidor en el puerto ${process.env.PORT}`)
    });
});

