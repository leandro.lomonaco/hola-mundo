const Lean = require("../models/Lean");


let AgregarRegistro = async (data) =>{
    let registro = new Lean({
        Nombre: data.Nombre,
        Apellido: data.Apellido
    });

    try{
        let respuesta = await registro.save();
        return {status: 200, texto: respuesta}
    }catch(ex){
        return {status: 500, texto: ex}
    }
}

let BuscarRegistro = async (nombre) =>{
    let registro = await Lean.find({Nombre:{ $regex: `.*${nombre}.*`}});
    return registro;
}
module.exports = {AgregarRegistro, BuscarRegistro};